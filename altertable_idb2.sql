﻿-- Renomeando o nome da tabela
ALTER TABLE teste.usuario RENAME TO usuario_login
-- Renomeando o campo nome completo
ALTER TABLE teste.usuario RENAME COLUMN "nome completo" TO nome_completo;

-- Alterar tipo de campo
ALTER TABLE teste.usuario_login ALTER COLUMN usuario TYPE char(10);

-- Alterar campo e determinar indic
ALTER TABLE teste.usuario_login ADD CONSTRAINT usuario_unico UNIQUE(usuario);

-- Dropando uma restrição
-- Testar antes de usar 
ALTER TABLE teste.usuario_login DROP CONSTRAINT usuario_pk;

-- Recriando a restrição
-- Testar antes de usar
ALTER TABLE teste.usuario_login ADD CONSTRAINT usuario_pk PRIMARY KEY(id);

-- Criando restrição para chave estrangeira
ALTER TABLE teste.usuario_login ADD CONSTRAINT usuario_fk FOREIGN KEY(id) REFERENCES usuario_login; -- Referencia obrigatoriamente a chave primária
ALTER TABLE teste.usuario_login ADD CONSTRAINT usuario_fk FOREIGN KEY(id) REFERENCES usuario_login(nome); -- Referencia qualquer campo entre as chaves.

ALTER TABLE teste.usuario_login ADD CHECK (usuario <> ' '); -- Não permite inserir espaço como nome de usuário