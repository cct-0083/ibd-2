﻿-- Script de criação de schema
create schema if not exists teste;

-- Script de criação do banco de dados

create table teste.usuario(
  id serial,
  usuario varchar(10),
  senha text);

create table teste.modulo(
  id serial,
  descricao varchar(60),
  url text -- tamanho ilimitado de string -- 
  );  

-- tabela de relacionamento entre usuário e módulo
create table teste.usuario_modulo(
  id_usuario integer,
  id_modulo integer );

-- Acrescentando alguns elementos nas tabelas

-- Alterar o nome da tabela
ALTER TABLE teste.modulo RENAME TO modulo;

-- Criar chave primária     
ALTER TABLE teste.usuario ADD CONSTRAINT usuario_pk PRIMARY KEY (id);

ALTER TABLE teste.modulo ADD CONSTRAINT modulo_pk PRIMARY KEY (id);

-- Criar restrições para os campos da tabela usuario
ALTER TABLE teste.usuario ADD CONSTRAINT usuario_unico UNIQUE(usuario);

-- Apagar a constraint usuario_usuario_check;
ALTER TABLE teste.usuario DROP CONSTRAINT usuario_usuario_check;

-- Verificar o tamanho do nome do usuario > 4 caracteres
ALTER TABLE teste.usuario ADD CONSTRAINT usuario_check CHECK (char_length(usuario) > 4);

-- Se for apagar a constraint usuario_usuario_check;
-- Por enquanto, não apague
ALTER TABLE teste.usuario DROP CONSTRAINT usuario_check;

-- Fazendo com que o campo de usuario não aceite nulo;
ALTER TABLE teste.usuario ALTER COLUMN usuario SET NOT NULL;

-- Fazendo com que o campo de senha não fique nulo;
ALTER TABLE teste.usuario ALTER COLUMN senha SET NOT NULL;

-- Determinando a senha padrão para qualquer usuário
-- Armazena um hash ao invez de uma senha
ALTER TABLE teste.usuario ALTER COLUMN senha SET DEFAULT MD5('12345');

-- Exemplo de inserção de dados para usuario
INSERT INTO teste.usuario(usuario) values('Adriana');

-- Selecionar os dados de adriana
-- Até o momento só tem a Adriana
select * from teste.usuario;

-- Retirando o valor default (padrão) para a senha
ALTER TABLE teste.usuario ALTER COLUMN senha DROP DEFAULT;

-- Retirando o not null para a senha
ALTER TABLE teste.usuario ALTER COLUMN senha DROP NOT NULL;

-- Agora a tabela de módulos

-- Fazer com que o campo descricao não seja nulo
ALTER TABLE teste.modulo ALTER COLUMN descricao SET NOT NULL;

-- Fazer com que o campo url não seja nulo
ALTER TABLE teste.modulo ALTER COLUMN url SET NOT NULL;

-- Fazer com que um modulo seja unico
ALTER TABLE teste.modulo ADD CONSTRAINT modulo_unico UNIQUE(descricao);

-- Testando a insercao de um modulo
INSERT INTO teste.modulo(descricao,url) values('cadastro','/app/cadastro');

-- Fazendo o relacionamento entre modulo e usuario

-- Aqui a chave primária possui dois campos
ALTER TABLE teste.usuario_modulo ADD CONSTRAINT usuario_modulo_pk PRIMARY KEY(id_usuario, id_modulo);

-- As chaves primárias aqui também são chaves estrangeiras, portanto:
ALTER TABLE teste.usuario_modulo ADD CONSTRAINT usuario_fk FOREIGN KEY (id_usuario) REFERENCES teste.usuario;
ALTER TABLE teste.usuario_modulo ADD CONSTRAINT modulo_fk FOREIGN KEY (id_modulo) REFERENCES teste.modulo;

-- Associando o modulo cadastro ao usuário de adriana
INSERT INTO teste.usuario_modulo values(1,1);


