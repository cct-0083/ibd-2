create table cliente (
	id serial,
	nome varchar(60),
	primary key (id)
)

create table compra (
    id serial,
	descricao varchar(60),
	primary key (id)
)

-- alterando a constraint da chave primária
alter table cliente drop constraint cliente_pkey;

-- criando nova constraint (restrição semântica)
alter table cliente add constraint cliente_pk primary key (id);

-- adicionando o campo que fará o papel de chave estrangeira

alter table compra add cliente_id integer;

-- adicionando o campo valor da compra 
alter table compra add valor numeric(5,3);

-- alterando os campos descricao e valor para not null
alter table compra alter column descricao set not null;
alter table compra alter column valor set not null;

-- criando constraint para as chaves estrangeira em compras
alter table compra add constraint compra_fk foreign key (cliente_id) references cliente;

-- criando um domínio (valores aceitaveis por um campo)
create domain string as varchar;

-- agora você pode usar "string" para substituir varchar.

alter table cliente alter column nome type string;

-- Inserindo dados na tabela cliente
insert into cliente(nome) values ('Alessandra'),('Lorena'),('Roberto');

-- Inserindo usando o sequence direto
-- nextval é uma função do postgres que pede o sequenciador por parâmetro (string)
insert into cliente values(nextval('cliente_id_seq'),'Carlos');

-- Para saber qual o valor do sequenciador 
-- currval ou seja current value -> currval
-- tem a função setval que seta o valor de uma sequence

select 'Valor atual do sequenciador de clientes ' || currval('cliente_id_seq');

-- inserindo 4 compras 
insert into compra(descricao,valor,cliente_id) values ('Arroz',3.87,1),('Feijão',47.48,2),('Cebola',50.781,1),
('Arroz',4.78,3)

-- inserindo + uma compra
insert into compra(descricao,valor,cliente_id) values ('Cebola do himaláia',5730.87,2)
-- Acontece um erro, porque?

-- corrigindo o problema
alter table compra alter column valor type numeric(6,2)

-- Execute a inserção novamente!!

-- selecionando os dados de compras

select * from compra


